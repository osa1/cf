// http://codeforces.com/problemset/problem/282/A

#include <iostream>

using namespace std;

int main()
{
  unsigned n;
  cin >> n;

  int ret = 0;

  for (unsigned i = 0; i < n; i ++)
  {
    string cmd;
    cin >> cmd;

    if (cmd.find("++") != string::npos)
      ret++;
    else
      ret--;
  }

  cout << ret;

  return 0;
}
