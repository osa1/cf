// http://www.codeforces.com/problemset/problem/155/A

#include <iostream>

using namespace std;

int main(int argc, const char *argv[])
{
  int contests;
  cin >> contests;

  int min = 10001;
  int max = -1;

  int r = 0;
  for (int i = 0; i < contests; i++)
  {
    int p;
    cin >> p;

    if (i == 0) {
      min = max = p;
      continue;
    }

    if (p < min || p > max) {
      r++;
      if (p < min) min = p;
      if (p > max) max = p;
    }
  }

  cout << r;

  return 0;
}
