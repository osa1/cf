// http://codeforces.com/problemset/problem/296/C

#include <iostream>
#include <vector>

#define REP(var, start, end, step) \
  for (unsigned var = (start); var < (end); var += (step))

using namespace std;

typedef long long ll;

struct op
{
  unsigned from, to;
  ll val;
  op(unsigned from, unsigned to, ll val)
    : from(from), to(to), val(val) {}
  void apply(vector<ll> &v, int count)
  {
    v[from] += count * val;
    if (to + 1 < v.size())
      v[to+1] -= count * val;
  }
};

int main()
{
  unsigned arrlen, opcount, querycount;
  cin >> arrlen >> opcount >> querycount;

  // read initial array
  vector<ll> inits(arrlen+1, 0);
  REP(i, 1, arrlen+1, 1)
    cin >> inits[i];

  // read operations
  vector<op*> ops(opcount, nullptr);
  REP(i, 0, opcount, 1)
  {
    unsigned from, to;
    ll val;
    cin >> from >> to >> val;
    ops[i] = new op(from, to, val);
  }

  // read queries
  vector<int> opcounts(opcount, 0);
  REP(i, 0, querycount, 1)
  {
    unsigned from, to;
    cin >> from >> to;
    opcounts[from-1]++;
    if (to < opcounts.size())
      opcounts[to]--;
  }

  // apply operations
  vector<ll> v(arrlen+1, 0);
  ll accum = 0;
  REP(i, 0, opcount, 1)
  {
    accum += opcounts[i];
    ops[i]->apply(v, accum);
  }

  // print array
  accum = 0;
  REP(i, 1, v.size(), 1)
  {
    accum += v[i];
    cout << inits[i] + accum << " ";
  }

  return 0;
}
