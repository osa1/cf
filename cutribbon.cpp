// http://codeforces.com/problemset/problem/189/A

#include <iostream>
#include <vector>

using namespace std;

int find_max(vector<int> &maxs)
{
  int r = -1;
  for (int i : maxs)
    if (i > r)
      r = i;
  return r;
}

int main()
{
  int len, a, b, c;
  cin >> len >> a >> b >> c;

  vector<int> t(len+1, -1);
  if (a <= len)
    t[a] = 1;
  if (b <= len)
    t[b] = 1;
  if (c <= len)
    t[c] = 1;

  vector<int> maxs;
  for (int i = 1; i <= len; i++)
  {
    maxs.clear();

    if (i-a > 0 && t[i-a] != -1)
      maxs.push_back(t[i-a]);
    if (i-b > 0 && t[i-b] != -1)
      maxs.push_back(t[i-b]);
    if (i-c > 0 && t[i-c] != -1)
      maxs.push_back(t[i-c]);

    int max = find_max(maxs)+1;
    if (max != 0 && max > t[i])
      t[i] = max;
  }

  cout << t[len];

  return 0;
}
