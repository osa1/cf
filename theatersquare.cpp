// http://www.codeforces.com/problemset/problem/1/A 

#include <cstdio>

int main()
{
  unsigned long long n, m, a;
  scanf("%llu %llu %llu", &n, &m, &a);

  unsigned long long h = n / a;
  if (n % a != 0) h++;

  unsigned long long w = m / a;
  if (m % a != 0) w++;

  printf("%llu", w * h);

  return 0;
}
