// http://codeforces.com/problemset/problem/92/A

#include <iostream>

using namespace std;

int main()
{
  int ws, chips;
  cin >> ws >> chips;

  int current_walrus = 1;

  while (chips - current_walrus >= 0)
  {
    chips -= current_walrus;
    if (current_walrus == ws)
      current_walrus = 1;
    else
      current_walrus++;
  }

  cout << chips;

  return 0;
}
