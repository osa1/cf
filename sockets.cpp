// http://codeforces.com/problemset/problem/257/A

#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
  unsigned filters, devices, sockets;
  cin >> filters >> devices >> sockets;

  vector<unsigned> filtercaps(filters);
  for (unsigned i = 0; i < filters; i++)
    cin >> filtercaps[i];

  sort(filtercaps.begin(), filtercaps.end());
  reverse(filtercaps.begin(), filtercaps.end());

  unsigned idx = 0;
  while (devices > sockets && idx < filters)
    sockets += filtercaps[idx++] - 1;

  if (devices > sockets)
    cout << "-1";
  else
    cout << idx;

  return 0;
}
