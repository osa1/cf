// http://www.codeforces.com/problemset/problem/82/A

#include <iostream>
#include <cmath>

#define MAX(a, b) (a < b ? a : b);

using namespace std;

int main(int argc, const char *argv[])
{
  unsigned input;
  cin >> input;

  unsigned df = 5;
  while (input > df)
  {
    input -= df;
    df *= 2;
  }

  df = df / 5;

  if (input <= df) {
    cout << "Sheldon";
    return 0;
  }
  input -= df;
  if (input <= df) {
    cout << "Leonard";
    return 0;
  }
  input -= df;
  if (input <= df) {
    cout << "Penny";
    return 0;
  }
  input -= df;
  if (input <= df) {
    cout << "Rajesh";
    return 0;
  }
  input -= df;
  if (input <= df) {
    cout << "Howard";
    return 0;
  }

  return 0;
}
