// http://www.codeforces.com/problemset/problem/4/A

#include <cstdio>

int main()
{
  int weight;
  scanf("%d", &weight);

  if (weight % 2 != 0) {
    printf("NO");
    return 0;
  }

  if (weight >= 4) {
    printf("YES");
    return 0;
  }

  printf("NO");

  return 0;
}
