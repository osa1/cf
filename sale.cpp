// http://www.codeforces.com/problemset/problem/34/B

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(int argc, const char *argv[])
{
  int tvc, bobc;
  cin >> tvc >> bobc;

  vector<int> tvs(tvc);
  for (int i = 0; i < tvc; i++)
    cin >> tvs[i];

  sort(tvs.begin(), tvs.end());

  unsigned r = 0;
  for (int i : tvs)
  {
    if (bobc == 0)
      break;
    if (i < 0) {
      r += -i;
      bobc--;
    }
  }

  cout << r;

  return 0;
}
