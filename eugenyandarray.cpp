// http://www.codeforces.com/problemset/problem/302/A 

#include <cstdio>
#include <cstdlib>
#include <iostream>

using namespace std;


int main()
{
  unsigned long arrlength, inputs;
  scanf("%ld %ld", &arrlength, &inputs);

  unsigned long negatives = 0, positives = 0;

  for (unsigned long i = 0; i < arrlength; i++)
  {
    short temp;
    scanf("%hd", &temp);
    if (temp == -1)
      negatives++;
    else
      positives++;
  }

  for (unsigned long i = 0; i < inputs; i++)
  {
    unsigned long start, end;
    cin >> start;
    cin >> end;

    if ((end-start+1) % 2 != 0) {
      printf("0\n");
    } else if (negatives >= (end-start+1) / 2 and positives >= (end-start+1) / 2) {
      printf("1\n");
    } else {
      printf("0\n");
    }
  }

  return 0;
}
