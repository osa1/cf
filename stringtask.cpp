// http://www.codeforces.com/problemset/problem/118/A

#include <iostream>
#include <ctype.h>

using namespace std;

int main(int argc, const char *argv[])
{
  string s;
  cin >> s;

  for (auto c : s) 
  {
    c = tolower(c);
    switch (c) {
      case 'a':
      case 'o':
      case 'y':
      case 'e':
      case 'u':
      case 'i':
        continue;
      default:
        cout << '.';
        cout << c;
        continue;
    }
  }

  return 0;
}
