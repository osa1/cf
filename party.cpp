// http://www.codeforces.com/problemset/problem/116/C
// http://www.codeforces.com/problemset/problem/115/A
// (same problems)

#include <iostream>
#include <vector>
#include <stack>

using namespace std;

int dfs(int m[2000], int i, int size)
{
  int r = 0;

  stack<int> s;
  s.push(i); 
  while (!s.empty())
  {
    int i = s.top();
    s.pop();
    if (m[i] != -1) {
      s.push(m[i]);
      r++;
    }
  }

  return r;
}

int main(int argc, const char *argv[])
{
  int ppl;
  cin >> ppl;

  int *m = new int[2000];

  for (int i = 0; i < ppl; i++)
  {
    int emp;
    cin >> emp;
    if (emp == -1) m[i] = -1;
    else m[i] = emp-1;
  }

  int max = 0;
  for (int i = 0; i < ppl; i++)
  {
    int r = dfs(m, i, ppl);
    if (r > max) max = r;
  }

  cout << max + 1;

  return 0;
}
