// http://codeforces.com/problemset/problem/127/B

#include <iostream>
#include <vector>

using namespace std;

int main(int argc, const char *argv[])
{
  int cs;
  cin >> cs;

  vector<int> sticks(100, 0);

  for (int i = 0; i < cs; i++)
  {
    int s;
    cin >> s;
    sticks[s]++;
  }

  int removed = 0;
  int i = 0;
  while (i < cs)
  {
    if (sticks[i] >= 2) {
      sticks[i] -= 2;
      removed++;
    } else {
      i++;
    }
  }

  cout << removed / 2;

  return 0;
}
