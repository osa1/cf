// http://www.codeforces.com/problemset/problem/24/A

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

template<typename T>
struct matrix
{
  matrix(unsigned _size, T init) : _size(_size), vs(_size*_size, init) {}
  T& operator ()(unsigned i, unsigned j) { return vs[i + _size * j]; }
  unsigned size() { return _size; }
private:
  unsigned _size;
  std::vector<T> vs;
};

// (total cost, redirection cost)
pair<int, int> *costs(matrix<int> &m, int node)
{
  for (int i = 0; i < m.size(); i++)
  {
    int road = m(node, i);
    if (road == -1)
      continue;
    else if (road == 0) {
      int t = m(i, node);
      int r = 0;
      m(i, node) = m(node, i) = -1;
      auto *cost_rest = costs(m, i);
      auto ret = new pair<int, int>(cost_rest->first + t, cost_rest->second + r);
      delete cost_rest;
      return ret;
    } else {
      int t = m(node, i);
      int r = m(node, i);
      m(i, node) = m(node, i) = -1;
      auto *cost_rest = costs(m, i);
      auto ret = new pair<int, int>(cost_rest->first + t, cost_rest->second + r);
      delete cost_rest;
      return ret;
    }
  }
  return new pair<int, int>(0, 0);
}

int main(int argc, const char *argv[])
{
  int rcount;
  cin >> rcount;
  matrix<int> m(rcount, -1);

  for (int i = 0; i < rcount; i++)
  {
    int from, to, cost;
    cin >> from >> to >> cost;
    m(from-1, to-1) = 0;
    m(to-1, from-1) = cost;
  }

  auto ret = costs(m, 0);
  int total_cost = ret->first;
  int one_way = ret->second;

  if (one_way < total_cost - one_way)
    cout << one_way;
  else
    cout << total_cost - one_way;

  return 0;
}
