// http://www.codeforces.com/problemset/problem/119/A

#include <iostream>

using namespace std;

int gcd(int a, int b)
{
  int temp;
  while (b != 0)
  {
    temp = b;
    b = a % temp;
    a = temp;
  }
  return a;
}

int main(int argc, const char *argv[])
{
  int simon, antis, stones;
  cin >> simon >> antis >> stones;

  int turn = 0;
  int _gcd;
  while (true)
  {
    if (turn == 0) {
      _gcd = gcd(stones, simon);
      if (stones < _gcd) {
        cout << 1;
        return 0;
      }
      stones -= _gcd;
      turn = 1;
    } else {
      _gcd = gcd(stones, antis);
      if (stones < _gcd) {
        cout << 0;
        return 0;
      }
      stones -= _gcd;
      turn = 0;
    }
  }

  return 0;
}
