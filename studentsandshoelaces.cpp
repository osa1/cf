// http://www.codeforces.com/problemset/problem/129/B

#include <iostream>
#include <vector>

using namespace std;

int total = 0;

void remove_sts(vector< vector<int> > &matrix)
{
  vector<int> removed;
  for (unsigned i = 0; i < matrix.size(); i++)
  {
    int connected = 0;
    int connectedTo = 0;
    for (unsigned j = 0; j < matrix[i].size(); j++)
    {
      if (matrix[i][j] != 0) {
        connected++;
        connectedTo = j;
      }
    }
    if (connected == 1) {
      removed.push_back(i);
      removed.push_back(connectedTo);
    }
  }

  if (removed.size()) {

    for (int i = 0; i < removed.size()-1; i+=2)
      matrix[removed[i]][removed[i+1]] = matrix[removed[i+1]][removed[i]] = 0;
    total++;
    remove_sts(matrix);
  }
}

int main(int argc, const char *argv[])
{
  int students, laces;
  cin >> students >> laces;

  vector<int> matrix_col(students, 0);
  vector< vector<int> > matrix(students, matrix_col);

  for (int i = 0; i < laces; i++)
  {
    int from, to;
    cin >> from >> to;
    matrix[from-1][to-1] = matrix[to-1][from-1] = 1;
  }

  remove_sts(matrix);

  cout << total;

  return 0;
}
