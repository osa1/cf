// http://www.codeforces.com/problemset/problem/71/A

#include <iostream>

using namespace std;

int main(int argc, const char *argv[])
{
  int words = 0;
  cin >> words;

  for (int i = 0; i < words; i++)
  {
    string s;
    cin >> s;

    if (s.length() <= 10)
      cout << s << endl;
    else
      cout << s[0] << s.length()-2 << s[s.length()-1] << endl;
  }

  return 0;
}
