// http://www.codeforces.com/problemset/problem/279/B

#include <iostream>
#include <vector>

using namespace std;

int main(int argc, const char *argv[])
{
  long timetoread, books;
  cin >> books >> timetoread;

  vector<int> booktimes(books);

  for (long i = 0; i < books; i++)
    cin >> booktimes[i];

  long total_time = 0;
  long total_books = 0;
  long max_seen = 0;

  long i = 0;
  long j = 1;
  while (j <= books)
  {
    total_time += booktimes[j-1];
    total_books++;

    while (total_time > timetoread && i < j)
    {
      total_time -= booktimes[i++];
      total_books--;
    }

    if (total_time <= timetoread && total_books > max_seen)
      max_seen = total_books;

    j++;
  }


  cout << max_seen;
  
  return 0;
}
