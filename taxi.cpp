// http://www.codeforces.com/problemset/problem/158/B

#include <iostream>

using namespace std;

int main(int argc, const char *argv[])
{
  int total = 0;

  int three = 0;
  int two = 0;
  int one = 0;

  int n;
  cin >> n;

  for (int i = 0; i < n; i++)
  {
    int s;
    cin >> s;

    if (s == 4) total++;
    else if (s == 3) three++;
    else if (s == 2) two++;
    else if (s == 1) one++;
  }

  total += three;
  one -= three;

  if (one < 0) one = 0;
  total += (one + (two*2) + 3) / 4;
  cout << total;

  return 0;
}
