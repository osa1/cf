// http://codeforces.com/problemset/problem/218/C

#include <iostream>
#include <vector>
#include <stack>
#include <set>

using namespace std;

struct node
{
  set<node*> neighbors;
  bool visited;
  unsigned x, y;
  node(unsigned x, unsigned y)
    : neighbors(), visited(false), x(x), y(y) {}
};

bool dfs(vector<node*> &nodevec, unsigned idx)
{
  stack<node*> s;

  if (nodevec[idx]->visited)
    return false;

  s.push(nodevec[idx]);

  while (!s.empty())
  {
    node *n = s.top();
    s.pop();

    if (n->visited)
      continue;
    n->visited = true;

    for (node *neighbor : n->neighbors)
      s.push(neighbor);
  }

  return true;
}

int main()
{
  unsigned nodes;
  cin >> nodes;

  vector<node*> nodevec;

  for (unsigned i = 0; i < nodes; i++)
  {
    unsigned xi, yi;
    cin >> xi >> yi;

    node *newnode = new node(xi, yi);

    for (node *n : nodevec)
      if (n->x == xi || n->y == yi)
      {
        newnode->neighbors.insert(n);
        n->neighbors.insert(newnode);
      }

    nodevec.push_back(newnode);
  }

  unsigned dfs_count = 0;
  for (unsigned i = 0; i < nodevec.size(); i++)
    if (dfs(nodevec, i))
      dfs_count++;

  cout << dfs_count - 1;

  return 0;
}
