// http://codeforces.com/problemset/problem/218/B

#include <iostream>
#include <vector>
#include <set>

using namespace std;

struct CompareLess
{
  bool operator()(const int i1, const int i2)
  {
    return i1 < i2;
  }
};

struct CompareGreater
{
  bool operator()(const int i1, const int i2)
  {
    return i1 > i2;
  }
};

template<typename T, typename T2>
void reorder(set<T, T2> &s, const T &e)
{
  s.erase(e);
  s.insert(e);
}

int main()
{
  multiset<int, CompareLess> minheap;
  multiset<int, CompareGreater> maxheap;

  unsigned ppl, planes;
  cin >> ppl >> planes;

  for (unsigned i = 0; i < planes; i++)
  {
    int seats;
    cin >> seats;
    minheap.insert(seats);
    maxheap.insert(seats);
  }

  int mintotal = 0;
  int maxtotal = 0;

  for (unsigned i = 0; i < ppl; i++)
  {
    int maxslot = *maxheap.begin();
    maxtotal += maxslot;
    maxheap.erase(maxheap.begin());
    if (maxslot > 1)
      maxheap.insert(maxslot-1);

    int minslot = *minheap.begin();
    while (minslot == 0 && !minheap.empty())
    {
      minheap.erase(minheap.begin());
      minslot = *minheap.begin();
    }
    mintotal += minslot;
    minheap.erase(minheap.begin());
    if (minslot > 1)
      minheap.insert(minslot-1);
  }

  cout << maxtotal << " " << mintotal;

  return 0;
}
