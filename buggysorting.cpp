// http://codeforces.com/contest/246/problem/A

#include <iostream>

using namespace std;

int main(int argc, const char *argv[])
{
  int n;
  cin >> n;

  if (n <= 2)
    cout << "-1";
  else {
    cout << "2 3 1";

    for (int i = 0; i < n - 3; i++)
      cout << " 3";
  }

  return 0;
}
