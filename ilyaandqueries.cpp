// http://codeforces.com/problemset/problem/313/B

#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

int main()
{
  vector<unsigned> v;
  v.push_back(0);

  char charbefore = getchar();

  char c;
  unsigned idx = 1;
  while ((c = getchar()) != '\n')
  {
    v.push_back(v[idx-1]);
    if (c == charbefore)
      v[idx]++;
    charbefore = c;
    idx++;
  }

  unsigned qc;
  cin >> qc;

  for (unsigned i = 0; i < qc; i++)
  {
    unsigned from, to;
    cin >> from >> to;
    cout << v[to-1] - v[from-1] << endl;
  }

  return 0;
}
