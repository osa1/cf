// http://codeforces.com/problemset/problem/166/E

#include <iostream>
#include <vector>

using namespace std;

int main()
{
  long long d = 0;
  long long a = 1;

  unsigned input;
  cin >> input;

  for (unsigned i = 0; i < input-1; i++)
  {
    long long at = a;
    long long dt = d;

    d = (at * 3) % 1000000007;
    a = (at * 2 + dt) % 1000000007;
  }

  cout << d;

  return 0;
}
