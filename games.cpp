// http://www.codeforces.com/problemset/problem/268/A

#include <iostream>
#include <vector>

using namespace std;

int main(int argc, const char *argv[])
{
  int teams;
  cin >> teams;

  vector<int> homes(teams);
  vector<int> guests(teams);

  for (int i = 0; i < teams; i++)
    cin >> homes[i] >> guests[i];

  int r = 0;

  for (int i = 0; i < teams; i++)
    for (int j = 0; j < teams; j++)
      if (homes[i] == guests[j])
        r++;

  cout << r;

  return 0;
}
