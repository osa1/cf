// http://www.codeforces.com/problemset/problem/158/A

#include <iostream>

using namespace std;

int main(int argc, const char *argv[])
{
  int n, k;

  cin >> n;
  cin >> k;

  int r = 0;
  int kth = 0;


  for (int i = 1; i <= n; i++)
  {
    int p;
    cin >> p;

    if (k-i == 0) kth = p;

    if (p > 0 && k-i >= 0) r++;
    else if (p > 0 && p >= kth) r++;
  }

  cout << r;

  return 0;
}
