// http://www.codeforces.com/problemset/problem/292/B

#include <iostream>
#include <vector>

using namespace std;

int main()
{
  int edges, nodes;
  cin >> nodes >> edges;

  vector<int> v(nodes, 0);

  for (int i = 0; i < edges; i++)
  {
    int from, to;
    cin >> from >> to;
    from--;
    to--;
    v[from]++;
    v[to]++;
  }

  int connected_to_two = 0;
  int connected_to_one = 0;
  int connected_to_many = 0;

  for (unsigned i = 0; i < v.size(); i++)
  {
    if (v[i] == 1) connected_to_one++;
    else if (v[i] == 2) connected_to_two++;
    else if (v[i] > 2) connected_to_many++;
  }

  if (connected_to_one == 0 && connected_to_many == 0)
    cout << "ring topology";
  else if (connected_to_many == 1 && connected_to_two == 0)
    cout << "star topology";
  else if (connected_to_one == 2 && connected_to_many == 0)
    cout << "bus topology";
  else
    cout << "unknown topology";

  return 0;
}
